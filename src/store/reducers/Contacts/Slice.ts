import { createSlice, isPending, isRejected, PayloadAction } from '@reduxjs/toolkit'
import { IContact } from '../../../models/IContact'
import {
   fetchContacts,
   createContact,
   deleteContact,
   editContact,
} from './ActionCreators'

interface ContactsState {
   contacts: IContact[]
   isContactsLoading: boolean
   error: string
   page: number
   limit: number
   totalCount: number
}

const initialState: ContactsState = {
   contacts: [],
   isContactsLoading: false,
   error: '',
   page: 1,
   limit: 5,
   totalCount: 0,
}

export const contactsSlice = createSlice({
   name: 'contacts',
   initialState,
   reducers: {
      setPageContact(state: ContactsState, action: PayloadAction<number>) {
         state.isContactsLoading = true
         state.page = action.payload
      },
      setLimitContact(state: ContactsState, action: PayloadAction<number>) {
         state.isContactsLoading = true
         state.limit = action.payload
      },
      setTotalCountContact(state: ContactsState, action: PayloadAction<number>) {
         state.totalCount = action.payload
      },
   },
   extraReducers: builder => {
      builder
         .addCase(fetchContacts.fulfilled, (state, action) => {
            state.contacts = action.payload
            state.isContactsLoading = false
            state.error = ''
         })
         .addCase(createContact.fulfilled, (state, action) => {
            state.isContactsLoading = false
            state.contacts.unshift(action.payload)
         })
         .addCase(deleteContact.fulfilled, (state, action) => {
            state.isContactsLoading = false
            state.contacts = state.contacts.filter(
               contact => contact.id !== action.payload
            )
            state.error = ''
         })
         .addCase(editContact.fulfilled, (state, action) => {
            state.contacts = state.contacts.map(contact => {
               if (contact.id == action.payload.id) {
                  return action.payload
               } else {
                  return contact
               }
            })

            state.isContactsLoading = false
            state.error = ''
         })
         .addMatcher(isPending, state => {
            state.isContactsLoading = true
            state.error = ''
         })
         .addMatcher(isRejected, (state, action) => {
            state.error = action.payload as string
            state.isContactsLoading = false
            state.error = ''
         })
   },
})

export default contactsSlice.reducer
export const { setPageContact, setLimitContact, setTotalCountContact } =
   contactsSlice.actions
