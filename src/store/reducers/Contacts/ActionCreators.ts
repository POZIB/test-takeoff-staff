import { createAsyncThunk } from '@reduxjs/toolkit'
import { setTotalCountContact } from './Slice'
import { IContact } from './../../../models/IContact'
import ContactsAPI from '../../../API/ContactsAPI'

interface IFetchContact {
   name?: string
   page?: number
   limit?: number
}

export const fetchContacts = createAsyncThunk(
   'contacts/fetchContacts',
   async function ({ name, page, limit }: IFetchContact, thunkAPI) {
      try {
         const response = await ContactsAPI.fetch(name, page, limit)
         const totalCount = response.headers['x-total-count']

         thunkAPI.dispatch(setTotalCountContact(Number(totalCount)))

         return response.data
      } catch (error) {
         return thunkAPI.rejectWithValue('Не удалось загрузить контакты')
      }
   }
)

export const createContact = createAsyncThunk(
   'contacts/createContact',
   async function (contact: IContact, thunkAPI) {
      try {
         const response = await ContactsAPI.create(contact)

         return response.data
      } catch (error) {
         return thunkAPI.rejectWithValue('Не удалось создать контакт')
      }
   }
)

export const deleteContact = createAsyncThunk(
   'contacts/deleteContact',
   async function (id: number, thunkAPI) {
      try {
         const response = await ContactsAPI.deleteById(id)
         return id
      } catch (error) {
         return thunkAPI.rejectWithValue('Не удалось удалить контакт')
      }
   }
)

export const editContact = createAsyncThunk(
   'contacts/editContact',
   async (contact: IContact, thunkAPI) => {
      try {
         const response = await ContactsAPI.edit(contact)

         return response.data
      } catch (error) {
         return thunkAPI.rejectWithValue(`Не удалось изменить контакт ${contact.name}`)
      }
   }
)
