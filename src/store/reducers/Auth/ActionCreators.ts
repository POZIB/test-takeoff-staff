import { createAsyncThunk } from '@reduxjs/toolkit'
import AuthService from '../../../API/AuthAPI'

interface IAuth {
   email: string
   password: string
}

export const login = createAsyncThunk(
   'users/login',
   async function ({ email, password }: IAuth, thunkAPI) {
      try {
         const response = await AuthService.login(email, password)

         if (!response.data.length) {
            return thunkAPI.rejectWithValue('Неверный логин или пароль!')
         } else {
            return response.data[0]
         }
      } catch (error) {
         return thunkAPI.rejectWithValue('Неверный логин или пароль!')
      }
   }
)

export const registration = createAsyncThunk(
   'users/registration',
   async function ({ email, password }: IAuth, thunkAPI) {
      try {
         const response = await AuthService.registration(email, password)

         return response.data
      } catch (error) {
         return thunkAPI.rejectWithValue((error as Error).message)
      }
   }
)
