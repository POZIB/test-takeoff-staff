import { createSlice, isPending, isRejected } from '@reduxjs/toolkit'
import { login, registration } from './ActionCreators'
import { IUser } from './../../../models/IUser'

interface AuthState {
   user: IUser | null
   isAuth: boolean
   isAuthLoading: boolean
   error: string
}

const initialState: AuthState = {
   user: null,
   isAuth: false,
   isAuthLoading: false,
   error: '',
}

export const authSlice = createSlice({
   name: 'auth',
   initialState,
   reducers: {
      setAuthError(state: AuthState) {
         state.error = ''
      },
      logout(state: AuthState) {
         localStorage.removeItem('user')
         state.error = ''
         state.isAuth = false
         state.isAuthLoading = false
         state.user = null
      },
      checkAuth(state: AuthState) {
         const user = localStorage.getItem('user')
         if (user) {
            state.user = JSON.parse(user) as IUser
            state.isAuth = true
            state.isAuthLoading = false
            state.error = ''
         }
      },
   },
   extraReducers: builder => {
      builder
         .addCase(login.fulfilled, (state, action) => {
            state.user = action.payload
            localStorage.setItem('user', JSON.stringify(action.payload))
            state.isAuth = true
            state.isAuthLoading = false
            state.error = ''
         })
         .addCase(registration.fulfilled, (state, action) => {
            state.user = action.payload
            localStorage.setItem('user', JSON.stringify(action.payload))
            state.isAuth = true
            state.isAuthLoading = false
            state.error = ''
         })
         .addMatcher(isPending, state => {
            state.isAuthLoading = true
            state.error = ''
         })
         .addMatcher(isRejected, (state, action) => {
            state.error = action.payload as string
            state.isAuthLoading = false
            state.isAuth = false
            state.error = ''
         })
   },
})

export default authSlice.reducer
export const { logout, checkAuth, setAuthError } = authSlice.actions
