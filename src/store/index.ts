import { combineReducers, configureStore } from '@reduxjs/toolkit'
import authSlice from './reducers/Auth/Slice'
import contactsSlice from './reducers/Contacts/Slice'

const rootReducer = combineReducers({
   auth: authSlice,
   contacts: contactsSlice,
})

export const configStore = () => {
   return configureStore({
      reducer: {
         auth: authSlice,
         contacts: contactsSlice,
      },
   })
}

export type RootState = ReturnType<typeof rootReducer>
export type AppStore = ReturnType<typeof configStore>
export type AppDispatch = AppStore['dispatch']
