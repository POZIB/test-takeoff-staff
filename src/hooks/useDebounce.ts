import { useEffect, useState } from 'react'

export default function useDebounce<T>(callback: T, delay: number = 350) {
   const [debouncedCallback, setDebouncedCallback] = useState<T>(callback)

   useEffect(() => {
      const timer = setTimeout(() => setDebouncedCallback(callback), delay)

      return () => {
         clearTimeout(timer)
      }
   }, [callback, delay])

   return debouncedCallback
}
