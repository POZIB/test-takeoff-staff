import { useEffect } from 'react'
import { Spin } from 'antd'
import { LoadingOutlined } from '@ant-design/icons'
import { useAppDispatch, useAppSelector } from './hooks/redux'
import { checkAuth } from './store/reducers/Auth/Slice'
import Header from './components/Header'
import AppRouter from './components/AppRouter'
import './App.css'

function App() {
   const dispatch = useAppDispatch()
   const { isAuthLoading } = useAppSelector(state => state.auth)

   useEffect(() => {
      dispatch(checkAuth())
   }, [])

   return (
      <div className='root_layout'>
         <Header />
         <main className='main'>
            <AppRouter />
         </main>
      </div>
   )
}

export default App
