import React, { FC, useState } from 'react'
import LoginForm from '../components/Authentications/LoginForm'
import RegistrationForm from '../components/Authentications/RegistrationForm'

const Authentication: FC = () => {
   const [logingOrRegister, setLogingOrRegister] = useState('login')

   return (
      <div className='authWrapper'>
         {logingOrRegister === 'login' ? (
            <LoginForm goToRegistration={setLogingOrRegister} />
         ) : (
            <RegistrationForm goToLogin={setLogingOrRegister} />
         )}
      </div>
   )
}

export default Authentication
