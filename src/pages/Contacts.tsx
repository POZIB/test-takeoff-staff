import React, { FC, useEffect, useState } from 'react'
import { Button, Input, Pagination, Spin } from 'antd'
import Modal from 'antd/lib/modal/Modal'
import { LoadingOutlined, SearchOutlined } from '@ant-design/icons'
import { useAppDispatch, useAppSelector } from '../hooks/redux'
import useDebounce from '../hooks/useDebounce'
import { setPageContact } from '../store/reducers/Contacts/Slice'
import { fetchContacts } from '../store/reducers/Contacts/ActionCreators'
import ContactItem from '../components/Contacts/ContactItem'
import CreateContact from '../components/Contacts/CreateContact'

const Contacts: FC = () => {
   const [searchByNameContact, setSearchByNameContact] = useState<string>('')
   const [isOpenCreateContact, setIsOpenCreateContact] = useState<boolean>(false)
   const debounce = useDebounce(searchByNameContact, 500)
   const dispatch = useAppDispatch()
   const { contacts, isContactsLoading, error, totalCount, page, limit } = useAppSelector(
      state => state.contacts
   )

   useEffect(() => {
      dispatch(fetchContacts({ name: searchByNameContact, page, limit }))
   }, [debounce, page])

   const handleChangePage = (number: number) => {
      dispatch(setPageContact(number))
   }

   const handleCreateContact = () => {
      setIsOpenCreateContact(true)
   }

   const handleSearchContact = (value: string) => {
      setSearchByNameContact(value)
   }

   const handleChangeLimitAndPage = (page: number, limit: number) => {
      dispatch(fetchContacts({ name: searchByNameContact, page, limit }))
   }

   return (
      <div className='contactsWrapper'>
         <div className='contactsWrapper__top'>
            <div className='top__inputs'>
               <div>
                  <Input
                     prefix={<SearchOutlined />}
                     placeholder='Поиск по имени'
                     value={searchByNameContact}
                     onChange={e => handleSearchContact(e.target.value)}
                  />
               </div>
               <div>
                  <Button type='primary' onClick={handleCreateContact}>
                     Создать новый
                  </Button>
               </div>
            </div>
            {error && <div className='wrapperError'>{error}</div>}
         </div>

         <div className='contactsWrapper__main'>
            <div className='containerContacts'>
               {isContactsLoading && (
                  <div className='loaderWrapper'>
                     <div className='loader'>
                        <Spin
                           indicator={<LoadingOutlined style={{ fontSize: 68 }} spin />}
                           delay={100}
                        />
                     </div>
                  </div>
               )}

               {contacts.length
                  ? contacts.map(contact => (
                       <ContactItem key={contact.id} contact={contact} />
                    ))
                  : !isContactsLoading && <div>Контактов нет</div>}
            </div>
         </div>

         <div className='contactsWrapper__bottom'>
            {limit < totalCount && (
               <Pagination
                  pageSizeOptions={['5', '10', '20']}
                  total={totalCount}
                  defaultPageSize={limit}
                  defaultCurrent={page}
                  showTotal={total => `Всего ${total}`}
                  showSizeChanger
                  onChange={handleChangePage}
                  onShowSizeChange={handleChangeLimitAndPage}
                  locale={{ items_per_page: '' }}
               />
            )}
         </div>

         <Modal
            visible={isOpenCreateContact}
            footer={false}
            destroyOnClose
            onCancel={() => setIsOpenCreateContact(false)}
         >
            <CreateContact onSubmit={() => setIsOpenCreateContact(false)} />
         </Modal>
      </div>
   )
}

export default Contacts
