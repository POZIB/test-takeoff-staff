import React from 'react'
import Authentication from '../pages/Authentication'
import Contacts from '../pages/Contacts'

export interface IRoute {
   path: string
   element: React.ComponentType
}

export enum RouteNames {
   REDIRECT = '*',
   AUTH = '/auth',
   CONTACTS = '/contacts',
}

export const publicRoutes: IRoute[] = [
   { path: RouteNames.REDIRECT, element: Authentication },
   { path: RouteNames.AUTH, element: Authentication },
]

export const privateRoutes: IRoute[] = [{ path: RouteNames.CONTACTS, element: Contacts }]
