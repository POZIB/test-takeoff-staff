export const rules = {
   required: (message: string = 'обязательное поле') => ({
      required: true,
      message,
   }),

   validationEmail: (message: string = 'неверный формат email') => ({
      type: 'email' as const,
      message: message,
   }),
}
