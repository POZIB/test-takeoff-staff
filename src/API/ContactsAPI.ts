import { AxiosResponse } from 'axios'
import { IContact } from '../models/IContact'
import $api from '../http'

export default class ContactsAPI {
   static async fetch(
      name?: string,
      page?: number,
      limit?: number
   ): Promise<AxiosResponse<IContact[]>> {
      return await $api.get<IContact[]>(
         `/contacts?name_like=${name}&_page=${page}&_limit=${limit}&_sort=id&_order=desc`
      )
   }

   static async create(contact: IContact): Promise<AxiosResponse<IContact>> {
      return await $api.post<IContact>(`/contacts`, {
         ...contact,
      })
   }

   static async deleteById(id: number): Promise<AxiosResponse<IContact[]>> {
      return await $api.delete<IContact[]>(`/contacts/${id}`)
   }

   static async edit(contact: IContact): Promise<AxiosResponse<IContact>> {
      return await $api.put<IContact>(`/contacts/${contact.id}`, contact)
   }
}
