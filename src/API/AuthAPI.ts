import { IUser } from './../models/IUser'
import { AxiosResponse } from 'axios'
import $api from '../http'

export default class AuthService {
   static async login(email: string, password: string): Promise<AxiosResponse<IUser[]>> {
      return await $api.get<IUser[]>(`/users?email=${email}&password=${password}`)
   }

   static async registration(
      email: string,
      password: string
   ): Promise<AxiosResponse<IUser>> {
      const response = await $api.get<IUser[]>(`/users?email=${email}`)

      if (response.data.length) {
         throw Error('Уже есть такой пользователь')
      } else {
         return await $api.post<IUser>('/users', { email, password })
      }
   }

   static async logout() {}
}
