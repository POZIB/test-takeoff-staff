export interface IContact {
   id?: number
   name: string
   telephone?: string
   address?: string
   telegram?: string
}
