import React, { FC } from 'react'
import { Route, Routes } from 'react-router-dom'
import { useAppSelector } from '../hooks/redux'
import { privateRoutes, publicRoutes } from '../router'

const AppRouter: FC = () => {
   const { isAuth } = useAppSelector(state => state.auth)

   return (
      <Routes>
         {publicRoutes.map(route => (
            <Route key={route.path} path={route.path} element={<route.element />} />
         ))}

         {isAuth &&
            privateRoutes.map(route => (
               <Route key={route.path} path={route.path} element={<route.element />} />
            ))}
      </Routes>
   )
}

export default AppRouter
