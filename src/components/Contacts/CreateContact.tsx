import React, { FC } from 'react'
import { Button, Form, Input } from 'antd'
import { useAppDispatch, useAppSelector } from '../../hooks/redux'
import { createContact } from '../../store/reducers/Contacts/ActionCreators'
import { rules } from '../../utils/rules'
import { IContact } from '../../models/IContact'

interface IProps {
   onSubmit: () => void
}

const CreateContact: FC<IProps> = props => {
   const dispatch = useAppDispatch()
   const { isContactsLoading } = useAppSelector(state => state.contacts)

   const onFinish = (contact: IContact) => {
      dispatch(createContact(contact))
      props.onSubmit()
   }

   return (
      <div className='contactForm'>
         <h2 className='contactForm__title'>Создание нового контакта</h2>
         <div className='contactFrorm__content'>
            <Form
               onFinish={onFinish}
               validateTrigger='onSubmit'
               labelCol={{ span: 4 }}
               wrapperCol={{ span: 16 }}
            >
               <Form.Item
                  name='name'
                  label='Имя'
                  rules={[rules.required('Введите имя!')]}
               >
                  <Input allowClear />
               </Form.Item>
               <Form.Item name='telephone' label='Телефон'>
                  <Input allowClear />
               </Form.Item>
               <Form.Item name='address' label='Адрес'>
                  <Input allowClear />
               </Form.Item>
               <Form.Item name='telegram' label='Телеграм'>
                  <Input allowClear />
               </Form.Item>
               <Form.Item
                  wrapperCol={{ span: 24, offset: 18 }}
                  style={{ marginBottom: 0 }}
               >
                  <Button type='primary' htmlType='submit' loading={isContactsLoading}>
                     Создать
                  </Button>
               </Form.Item>
            </Form>
         </div>
      </div>
   )
}

export default CreateContact
