import React, { FC, useState } from 'react'
import { Button, Modal } from 'antd'
import { useAppDispatch } from '../../hooks/redux'
import { deleteContact } from '../../store/reducers/Contacts/ActionCreators'
import { IContact } from '../../models/IContact'
import EditContact from './EditContact'

interface IProps {
   contact: IContact
}

const ContactItem: FC<IProps> = ({ contact }) => {
   const [isOpenEditContact, setIsOpenEditContact] = useState<boolean>(false)
   const dispatch = useAppDispatch()

   const confirm = () => {
      Modal.confirm({
         title: 'Подтверждение',
         content: `Вы действительно хотите удалить контакт ${contact.name}?`,
         okText: 'Да',
         cancelText: 'Отменить',
         onOk: () => {
            handleDeleteContact()
         },
      })
   }
   const handleDeleteContact = () => {
      if (contact.id) {
         dispatch(deleteContact(contact.id))
      }
   }

   const handleEditContact = () => {
      setIsOpenEditContact(true)
   }

   return (
      <div className='contactItem'>
         <div className='contactItem__body'>
            <div className='contactItem__number'>№{contact.id}</div>
            <div className='contactItem__itemContent'>
               <div className='itemContent__item'>Имя: {contact.name}</div>
               <div className='itemContent__item'>Телефон: {contact.telephone}</div>
               <div className='itemContent__item'>Адрес: {contact.address}</div>
               <div className='itemContent__item'>Телеграм: {contact.telegram}</div>
            </div>
            <div className='contactItem___buttons'>
               <Button onClick={handleEditContact} type='primary' ghost>
                  Редактировать
               </Button>
               <Button onClick={confirm} type='primary' danger>
                  Удалить
               </Button>
            </div>
         </div>
         <Modal
            visible={isOpenEditContact}
            onCancel={() => setIsOpenEditContact(false)}
            footer={false}
            destroyOnClose
         >
            <EditContact contact={contact} onSubmit={() => setIsOpenEditContact(false)} />
         </Modal>
      </div>
   )
}

export default ContactItem
