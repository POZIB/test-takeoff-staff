import { FC } from 'react'
import { Button, Form, Input } from 'antd'
import { useAppDispatch, useAppSelector } from '../../hooks/redux'
import { editContact } from '../../store/reducers/Contacts/ActionCreators'
import { IContact } from '../../models/IContact'
import { rules } from '../../utils/rules'

interface IProps {
   contact: IContact
   onSubmit: () => void
}

const EditContact: FC<IProps> = props => {
   const dispatch = useAppDispatch()
   const { isContactsLoading } = useAppSelector(state => state.contacts)

   const onFinish = (contact: IContact) => {
      contact.id = props.contact.id

      dispatch(editContact(contact))
      props.onSubmit()
   }

   return (
      <div className='contactForm'>
         <h2 className='contactForm__title'>
            Редактирование контакта "{props.contact.name}"
         </h2>
         <div className='contactFrorm__content'>
            <Form
               onFinish={onFinish}
               validateTrigger='onSubmit'
               initialValues={props.contact}
               labelCol={{ span: 4 }}
               wrapperCol={{ span: 16 }}
            >
               <Form.Item
                  name='name'
                  label='Имя'
                  rules={[rules.required('Введите имя!')]}
               >
                  <Input allowClear />
               </Form.Item>
               <Form.Item name='telephone' label='Телефон'>
                  <Input allowClear />
               </Form.Item>
               <Form.Item name='address' label='Адрес'>
                  <Input allowClear />
               </Form.Item>
               <Form.Item name='telegram' label='Телеграм'>
                  <Input allowClear />
               </Form.Item>
               <Form.Item
                  wrapperCol={{ span: 24, offset: 18 }}
                  style={{ marginBottom: 0 }}
               >
                  <Button type='primary' htmlType='submit' loading={isContactsLoading}>
                     Изменить
                  </Button>
               </Form.Item>
            </Form>
         </div>
      </div>
   )
}

export default EditContact
