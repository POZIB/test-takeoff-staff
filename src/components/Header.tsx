import { Avatar, Button } from 'antd'
import { UserOutlined } from '@ant-design/icons'
import React, { FC } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { useAppDispatch, useAppSelector } from '../hooks/redux'
import { logout } from '../store/reducers/Auth/Slice'

const Header: FC = () => {
   const navigate = useNavigate()
   const dispatch = useAppDispatch()
   const { isAuth, user } = useAppSelector(state => state.auth)

   const handlerLogout = () => {
      dispatch(logout())
   }

   const handlerAuth = () => {
      navigate('/auth')
   }

   return (
      <header className='header'>
         <div className='header__content'>
            <div className='header__leftBlock'>
               <div className='header__item'>
                  {isAuth && <Link to='/contacts'>Контакты</Link>}
               </div>
            </div>
            <div className='header__rightBlock'>
               {isAuth && (
                  <>
                     <div className='header__item'>
                        <div className='header__account'>
                           <Avatar className='account__avatar' icon={<UserOutlined />} />
                           <div className='account__userName'>{user?.email}</div>
                        </div>
                     </div>
                     <div className='header__item'>
                        <Button
                           className='account__btnLogout'
                           onClick={handlerLogout}
                           type='default'
                           ghost
                        >
                           Выйти
                        </Button>
                     </div>
                  </>
               )}

               {!isAuth && (
                  <div className='header_item'>
                     <Button onClick={handlerAuth} type='default' ghost>
                        Войти
                     </Button>
                  </div>
               )}
            </div>
         </div>
         <div></div>
      </header>
   )
}

export default Header
