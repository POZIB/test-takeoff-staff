import { Button, Form, Input } from 'antd'
import { UserOutlined, LockOutlined } from '@ant-design/icons'
import React, { FC, useEffect } from 'react'
import { useAppDispatch, useAppSelector } from '../../hooks/redux'
import { registration } from '../../store/reducers/Auth/ActionCreators'
import { setAuthError } from '../../store/reducers/Auth/Slice'
import { rules } from '../../utils/rules'

interface IProps {
   goToLogin: (event: string) => void
}

interface IAuth {
   email: string
   password: string
}

const RegistrationForm: FC<IProps> = props => {
   const [form] = Form.useForm()
   const { error, isAuthLoading, isAuth } = useAppSelector(state => state.auth)
   const dispatch = useAppDispatch()

   const submit = (values: IAuth) => {
      if (isAuth) return
      dispatch(registration(values))
   }

   useEffect(() => {
      if (isAuth) form.resetFields()

      return () => {
         dispatch(setAuthError())
      }
   }, [isAuth])

   return (
      <div className='authWrapper__form'>
         <h1 className='form__title'>Регистрация</h1>
         <Form form={form} onFinish={submit}>
            <Form.Item
               name='email'
               rules={[
                  rules.required('Введите email'),
                  rules.validationEmail('Неверный формат'),
               ]}
            >
               <Input
                  placeholder='Email'
                  prefix={<UserOutlined className='site-form-item-icon' />}
                  allowClear
               />
            </Form.Item>
            <Form.Item name='password' rules={[rules.required('Введите пароль')]}>
               <Input
                  type='password'
                  placeholder='Пароль'
                  prefix={<LockOutlined className='site-form-item-icon' />}
                  allowClear
               />
            </Form.Item>
            <Form.Item>
               <Button type='primary' htmlType='submit' loading={isAuthLoading} block>
                  Зарегистроваться
               </Button>
            </Form.Item>

            {error && <div className='wrapperError'>{error}</div>}
         </Form>
         <div className='from__link'>
            <a onClick={() => props.goToLogin('login')}>Войти</a>
         </div>
      </div>
   )
}

export default RegistrationForm
