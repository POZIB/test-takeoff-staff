import { Button, Form, Input } from 'antd'
import { UserOutlined, LockOutlined } from '@ant-design/icons'
import React, { FC, useEffect } from 'react'
import { useAppDispatch, useAppSelector } from '../../hooks/redux'
import { login } from '../../store/reducers/Auth/ActionCreators'
import { setAuthError } from '../../store/reducers/Auth/Slice'
import { rules } from '../../utils/rules'

interface IProps {
   goToRegistration: (event: string) => void
}

interface IAuth {
   email: string
   password: string
}

const LoginForm: FC<IProps> = props => {
   const [form] = Form.useForm()
   const { error, isAuthLoading, isAuth } = useAppSelector(state => state.auth)
   const dispatch = useAppDispatch()

   useEffect(() => {
      if (isAuth) form.resetFields()

      return () => {
         dispatch(setAuthError())
      }
   }, [isAuth])

   const onSubmit = (values: IAuth) => {
      if (isAuth) return
      dispatch(login(values))
   }

   return (
      <div className='authWrapper__form'>
         <h1 className='form__title'>Авторизация</h1>
         <Form form={form} onFinish={onSubmit}>
            <Form.Item name='email' rules={[rules.required('Введите email')]}>
               <Input
                  placeholder='Email'
                  prefix={<UserOutlined className='site-form-item-icon' />}
                  allowClear
               />
            </Form.Item>
            <Form.Item name='password' rules={[rules.required('Введите пароль')]}>
               <Input
                  placeholder='Пароль'
                  prefix={<LockOutlined className='site-form-item-icon' />}
                  type='password'
                  allowClear
               />
            </Form.Item>
            <Form.Item>
               <Button type='primary' htmlType='submit' loading={isAuthLoading} block>
                  Войти
               </Button>
            </Form.Item>

            {error && <div className='wrapperError'>{error}</div>}
         </Form>
         <div className='from__link'>
            <a onClick={() => props.goToRegistration('register')}>Зарегистроваться</a>
         </div>
      </div>
   )
}

export default LoginForm
